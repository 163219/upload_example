(function() {

    var UploadApp = angular.module("UploadApp", ["angularFileUpload"]);
    
    var UploadCtrl = function($http, FileUploader) {
        var vm = this;
        vm.uploader = new FileUploader({
            url: "/upload_multiple",
            alias: "myImage",
            removeAfterUpload: true
        });
        
        vm.uploader.onCompleteAll = function() {
            //Clear the input filed
            document.querySelector("input[type='file']").value = null;
        };
        
        vm.uploadOne = function() {
            vm.uploader.uploadAll();
        }
    };
    
    UploadApp.controller("UploadCtrl", ["$http", "FileUploader", UploadCtrl]);

})();