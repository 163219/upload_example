(function() {
    
    var ImageApp = angular.module("ImageApp", []);
    
    var ImageCtrl = function($http) {
        
        var vm = this;
        vm.images = [];
        
        $http.get("/api/images")
            .then( function(result) {
                console.info(">>> data = %s", JSON.stringify(result.data));
                vm.images = result.data;
            });
    }
    
    ImageApp.controller("ImageCtrl", ["$http", ImageCtrl]);
    
})();