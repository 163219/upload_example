var express = require("express");
var app = express();

var path = require("path");

//Specify the upload directory
const uploadDir = path.join(__dirname, "tmp/");
var multer = require("multer");
var uploader = multer({
    dest: uploadDir
});

var mysql = require("mysql");
const pool = mysql.createPool({
    hostname: "localhost",
    port: 3306,
    user: "fred",
    password: "fred",
    database: "mydb",
    connectionLimit: 2
});

var asset = require("./module/asset_management")(pool);

//Should be in a separate module
//insert function
var q = require("q");

app.get("/api/images", function(req, res) {
    asset.getImages()
        .then(function(result) {
            res.json(result);
        });
});

app.get("/image/:asset_id", function(req, res) {
    asset.getImage(req.params.asset_id)
        .then(function(result) {
            res.status(200)
                .type(result.image_type)
                .send(result.image);
        })
});

//Process request
app.post("/upload", uploader.single("myImage"), function(req, res) {
    //file.fieldname
    //file.mimetype
    //file.destination - directory
    //file.filename
    //file.path
    //file.size
    asset.insertImage(req.file)
        .then(function(result) {
            res.status(200)
                .type("text/plain")
                .send("Uploaded: " + result);
        }).catch(function(err) {
            res.status(400)
                .type("text/plain")
                .send("Error: " + err);
        });
});

app.post("/upload_multiple", uploader.array("myImage"), function(req, res) {
    //Not the best way to do this. Multiple images should probably be batch updated
    //which is more efficient
    var allInserts = [];
    for (var i in req.files)
        allInserts.push(asset.insertImage(req.files[i]));
    //Now wait for all the promises to be resolved
    q.all(allInserts)
        .then(function(result) {
            console.info(">>> resolved: %s", JSON.stringify(result));
            res.status(200)
                .type("text/plain")
                .send("Uploaded: " + JSON.stringify(result));
        }).catch(function(err) {
            res.status(400)
                .type("text/plain")
                .send("Error: " + err);
        });
});

app.use("/bower_components", express.static(path.join(__dirname, "bower_components")));
app.use("/", express.static(path.join(__dirname, "public")));

app.set('port', process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function() {
    console.info("Application started on port %d", app.get("port"));
});