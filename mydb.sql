CREATE TABLE asset (
  asset_id char(48) NOT NULL,
  image longblob NOT NULL,
  name varchar(48) NOT NULL,
  update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  size bigint(20) NOT NULL,
  image_type varchar(16) NOT NULL DEFAULT 'image/png',
  PRIMARY KEY (asset_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
